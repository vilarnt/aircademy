from setuptools import setup, find_packages


# with open("README", 'r') as f:
#     long_description = f.read()

setup(
    name='aircademy',
    version='0.4.0',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    url='https://gitlab.com/vilarnt/aircademy',
    license='',
    author='Vilar da Camara Neto',
    author_email='vilarneto@gmail.com',
    description='Django-inspired library to interface with Airtable',
    install_requires=[
        "deprecated",
        "furl>=2.0.0",
        "pytz",
        "requests>=2.22.0",
        "simplejson>=3.16.0",
    ],
    extras_require={
        "yaml": ["pyyaml>=5.1"],
    }
    # long_description=long_description,
)
